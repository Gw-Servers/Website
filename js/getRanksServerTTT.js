var rankType;
$(document).ready(function() {
  rankType = "kills";
  $("#player-ranks").hide().html("<div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
  $.post("getRanksServerTTT.php", {
    rankType: rankType
  }, function(result) {
    $("#player-ranks").hide().html(result).fadeIn();
  });
  $("#player-ranks2").hide().html("<div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
  $.post("getRanksServerTTT2.php", {
    rankType: rankType
  }, function(result) {
    $("#player-ranks2").hide().html(result).fadeIn();
  });

  getRanks();
  getRanks2();

});

function getRanks() {
  rankType = $("#rank-type-list").val();
  $("#rank-type-list").change(function() {
    rankType = $("#rank-type-list").val();
    $('#rank-type-list').attr('disabled', 'disabled');
    $("#player-ranks").hide().html("<div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
    $.post("getRanksServerTTT.php", {
      rankType: rankType
    }, function(result) {
      $("#player-ranks").hide().html(result).fadeIn();
      $('#rank-type-list').removeAttr('disabled');
    });
  });
}

function getRanks2() {
  rankType = $("#rank-type-list").val();
  $("#rank-type-list").change(function() {
    rankType = $("#rank-type-list").val();
    $('#rank-type-list').attr('disabled', 'disabled');
    $("#player-ranks2").hide().html("<div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"four columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
    $.post("getRanksServerTTT2.php", {
      rankType: rankType
    }, function(result) {
      $("#player-ranks2").hide().html(result).fadeIn();
      $('#rank-type-list').removeAttr('disabled');
    });
  });
}
