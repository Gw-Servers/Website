var rankType;
$( document ).ready(function() {
  rankType = "kills";
  $("#player-ranks-row1").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
  $.post("getRanksRow1.php", {rankType: rankType}, function(result){
      $("#player-ranks-row1").hide().html(result).fadeIn();
  });
  $("#player-ranks-row2").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
  $.post("getRanksRow2.php", {rankType: rankType}, function(result){
      $("#player-ranks-row2").hide().html(result).fadeIn();
  });
  $("#player-ranks-row3").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
  $.post("getRanksRow3.php", {rankType: rankType}, function(result){
      $("#player-ranks-row3").hide().html(result).fadeIn();
  });
  $("#player-ranks-row4").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
  $.post("getRanksRow4.php", {rankType: rankType}, function(result){
      $("#player-ranks-row4").hide().html(result).fadeIn();
  });
  $("#user-rank-section").hide().html("<div class=\"row user-rank-box\"><div class=\"row\"><h4>Loading Ranks</h4></div><div class=\"row center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"75px\"></div></div><div class=\"row\" id=\"user-score\"><div class=\"arrow-down-white\"></div><div class=\"center-text\">Loading Score</div></div>").fadeIn();
  $.post("getRanksUser.php", {rankType: rankType}, function(result){
    $("#user-rank-section").hide().html(result).fadeIn();
  });

  getRanksRow1();
  getRanksRow2();
  getRanksRow3();
  getRanksRow4();
  getRanksUser();

});

function getRanksRow1 () {
  rankType = $("#rank-type-list").val();
  $("#rank-type-list").change(function(){
    rankType = $("#rank-type-list").val();
    $('#rank-type-list').attr('disabled', 'disabled');
    $("#player-ranks-row1").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
    $.post("getRanksRow1.php", {rankType: rankType}, function(result){
        $("#player-ranks-row1").hide().html(result).fadeIn();
        $('#rank-type-list').removeAttr('disabled');
    });
  });
}

function getRanksRow2 () {
  rankType = $("#rank-type-list").val();
  $("#rank-type-list").change(function(){
    rankType = $("#rank-type-list").val();
    $("#player-ranks-row2").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
    $.post("getRanksRow2.php", {rankType: rankType}, function(result){
        $("#player-ranks-row2").hide().html(result).fadeIn();
    });
  });
}

function getRanksRow3 () {
  rankType = $("#rank-type-list").val();
  $("#rank-type-list").change(function(){
    rankType = $("#rank-type-list").val();
    $("#player-ranks-row3").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
    $.post("getRanksRow3.php", {rankType: rankType}, function(result){
        $("#player-ranks-row3").hide().html(result).fadeIn();
    });
  });
}

function getRanksRow4 () {
  rankType = $("#rank-type-list").val();
  $("#rank-type-list").change(function(){
    rankType = $("#rank-type-list").val();
    $("#player-ranks-row4").hide().html("<div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div><div class=\"three columns player-rank-box\"><div class=\"center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"130px\"><span class=\"loading-player\">Loading Player!</span></div></div>").fadeIn();
    $.post("getRanksRow4.php", {rankType: rankType}, function(result){
        $("#player-ranks-row4").hide().html(result).fadeIn();
    });
  });
}

function getRanksUser () {
  rankType = $("#rank-type-list").val();
  $("#rank-type-list").change(function(){
    rankType = $("#rank-type-list").val();
    $("#user-rank-section").hide().html("<div class=\"row user-rank-box\"><div class=\"row\"><h4>Loading Ranks</h4></div><div class=\"row center-text\"><img src=\"img/svg-loaders/tail-spin.svg\" class=\"responsive-img\" width=\"75px\"></div></div><div class=\"row\" id=\"user-score\"><div class=\"arrow-down-white\"></div><div class=\"center-text\">Loading Score</div></div>").fadeIn();
    $.post("getRanksUser.php", {rankType: rankType}, function(result){
        $("#user-rank-section").hide().html(result).fadeIn();
    });
  });
}


