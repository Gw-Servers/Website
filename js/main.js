var discountStart = 5;
var maxDiscount = 15;
var baseCost = 0.1;
var multiplier = 1;

$(document).ready(function() {

  getServerInfo();
  getServerFooterInfo();

  var windowHeight = $(window).height();
  var setHeight = windowHeight - 75;
  $(".modal-scroll").css("max-height", setHeight);
  $(window).resize(function () {
    windowHeight = $(window).height();
    setHeight = windowHeight - 75;
    $(".modal-scroll").css("max-height", setHeight);
  });

  tokens = $("#tokens").val();
  calcTokenCost(tokens);
  $("#tokens").change(function() {
    tokens = $("#tokens").val();
    calcTokenCost(tokens);
  });

  bob = $("#bobs").val();
  calcBobCost(bob);
  $("#bobs").change(function() {
    bob = $("#bobs").val();
    calcBobCost(bob);
  });
});

function calcBobCost(bob) {
  bob = parseInt(bob);
  tokenCost = bob * 100;
  tokenCost = Math.ceil(tokenCost);
  console.log(tokenCost);
  if (isNaN(tokenCost)) {
    $(".bob-button").html("Enter Numbers Only!");
  } else {
    $(".bob-button").html("<span id=\"bob-cost\">0</span> Bob");
    $("#bob-cost").html(tokenCost);
  }
}


function calcTokenCost(tokens) {
  parseInt(tokens);
  $("#paypal-quant").val(tokens);
  pricePerToken = 0.1;
  totalCost = pricePerToken*tokens;
  totalCost = totalCost.toFixed(2);
  if (totalCost == "NaN") {
    console.log("shit hit the fan");
    $(".token-button").html("Enter Numbers Only!");
  } else{
    $(".token-button").html("£<span id=\"token-cost\"></span>");
    $("#token-cost").html(totalCost);
  }
  
  /*
  $("#paypal-quant").val(tokens);
  if (tokens >= (discountStart / baseCost) * multiplier) {
    calculatedPercent = ((tokens - 50) / 10) + 1;
    calculatedPercent = Math.floor(calculatedPercent);
    calculatedPercent = calculatedPercent * multiplier;
  } else {
    calculatedPercent = 0;
  }
  if (calculatedPercent > maxDiscount) {
    calculatedPercent = 15;
  }
  totalCost = (tokens * baseCost) * (100 - calculatedPercent) / 100;
  totalCost = totalCost.toFixed(2);
  if (totalCost == "NaN") {
    console.log("shit hit the fan");
    $(".token-button").html("Enter Numbers Only!");
  } else {
    console.log("all is good");
    $(".token-button").html("£<span id=\"token-cost\"></span>");
    $("#token-cost").html(totalCost);
    pricePerToken = totalCost/tokens;
    console.log("before fixed :"+pricePerToken);
    pricePerToken = roundToTwo(pricePerToken);
    console.log("after fixed :"+pricePerToken);

    //console.log("before fixed :"+pricePerToken);
    //pricePerToken = Math.floor((pricePerToken/10)*10)
    //console.log("after fixed :"+pricePerToken);

    //console.log("before ceil :"+pricePerToken);
    //pricePerToken = Math.ceil(pricePerToken);
    //console.log("after ceil: "+pricePerToken);

    $("#paypal-val").val(roundToTwo(pricePerToken));
    
  }
  */
}

function getServerInfo() {
  $("#server-list").html("<div class=\"row margin-20-bottom margin-10-top\"><div class=\"row\"><div class=\"center-text\"><h6 class=\"no-margin-bottom sub-text-dark\"><strong>Loading Server!</strong></h6></div></div><div class=\"row\" style=\"height: 24px\"><div class=\"status-bar animate\"><span class=\"center-text\"><span></span></span></div><div class=\"status-bar-text\"><strong>Loading Players!</strong></div></div></div>");
  $.ajax({
    url: "getPlayers.php",
    success: function(result) {
      timerMins = 0;
      $("#server-list").html(result);
      $(".alive").hover(function() {
        originalDivContent = $(this).find(".status-bar-text").clone();
        $(this).find(".status-bar-text").html("<strong>Connect!</strong>");
      }, function() {
        $(this).find(".status-bar-text").replaceWith(originalDivContent);
      });
    },
    error: function() {
      $("#server-list").html("PHP Error, Ajax didn't work!");
    }
  });
}

function getServerFooterInfo() {
  $("#server-list-footer").html("<div class=\"server sub-text-light\">Loading Server!<div class=\"server-status-down\">Loading!</div></div>");
  $.ajax({
    url: "getPlayersFooter.php",
    success: function(result) {
      $("#server-list-footer").html(result);
    },
    error: function() {
      $("#server-list-footer").html("PHP Error, Ajax didn't work!");
    }
  });
}
