<?php

require 'php/phpmailer/PHPMailerAutoload.php';

		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		// Set PHPMailer to use the sendmail transport
		$mail->isSendmail();
		//Set who the message is to be sent from
		$mail->setFrom('getwreckedservers@gmail.com', 'Gw Servers');
		//Set who the message is to be sent to
		$mail->addAddress('whoto@example.com');
		//Set the subject line
		$mail->Subject = 'Get wrecked - Donation Complete!';
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML(file_get_contents('emails/payment_complete.php'));
		//Replace the plain text body with one created manually
		$mail->AltBody = 'Your donation is now complete! you should now have your tokens! Thank you.';
		//send the message, check for errors
		if (!$mail->send()) {
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
		    echo "Message sent!";
		}

?>