<?php
	$heroImages = [ //[0] = imgsrc, [1] = data-position(y), [2] = Hero text Colour(css class)(default is white)
		$heroImage1 = ["img/hero_bg01.jpg","center", ""],
		$heroImage2 = ["img/hero_bg02.jpg","bottom",""],
		$heroImage3 = ["img/hero_bg03.jpg","center",""],
    $heroImage4 = ["img/hero_bg04.jpg","center",""]
	];

	$randomImage = array_rand($heroImages, 1);
	$imageToDisplay = $heroImages[$randomImage][0];
	$imagePosition = $heroImages[$randomImage][1];
	$heroTextColour = $heroImages[$randomImage][2];
?>