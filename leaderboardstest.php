<?php
  error_reporting(E_ALL);
  require_once 'php/config.php';
  require 'randomHero.php';
  require 'getRanks.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Get wrecked! Servers - Leaderboards</title>
    <meta name="description" content="Get Wrecked Servers. Premium Garry's Mod servers and community!">
    <meta name="author" content="GwServers">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/main.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="img/favicon.png">

</head>
<body>

<section id="top-bar">
  <header class="container">
    <div class="row">
      <div class="three columns" id="logo">
        <a href="index.php"><img src="img/logo.png" height="50px"></a>
      </div>
      <div class="nine columns" id="navbar">
        <input type="checkbox" id="show-menu" role="button" value="Open/Close Menu" />
        <label for="show-menu" class="show-menu">Menu</label>
        <ul id="main-menu">
          <li><a href="#">Forums</a></li>
          <li><a href="#">Servers</a>
            <ul>
              <li><a href="trouble-in-terrorist-town-1.php">Trouble in Terrorist Town #1</a></li>
              <li><a href="#">Trouble in Terrorist Town #2</a></li>
              <li><a href="#">Survival [0-99]</a></li>
              <li><a href="#">Prophunt</a></li>
              <li><a href="leaderboards.php">Leaderboards</a></li>
            </ul>
          </li>
          <li><a href="#">Donate</a></li>
          <?php
            if ($mybb->user['uid']) {
              echo "
          <li><a href=\"../forums/usercp.php\">Account<img src=\"../forums/".$mybb->user['avatar']."\"class=\"responsive-img hide-on-break\" id=\"nav-user-img\" height=\"40px\"></a>
            <ul>
              <li><a href=\"#\">View Profile</a></li>
              <li><a href=\"#\">Settings</a></li>
              <li><a href=\"#\">Logout</a></li>
            </ul>
          </li>
              ";
            }
            else{
              echo "<li><a href=\"../forums/member.php?action=login\">Login</a></li>";
            }
          ?>
        </ul>
      </div>
    </div>
  </header>
</section>

<section id="hero" data-parallax="scroll" data-image-src="<?php echo $imageToDisplay; ?>" data-speed="0.6" data-position="0px <?php echo $imagePosition;?>">
  <div class="container">
    <div class="row">
      <div class="twelve columns center-text <?php echo $heroTextColour; ?>">
        <img src="img/logo.png" class="margin-20-top panther">
        <h1>Gamemode Leaderboards</h1>
      </div>
    </div>
  </div>
</section>

<section id="main-content">

  <div class="container padding-20-top">
    <div class="row content-box margin-20-bottom">
        <h2>Trouble in Terrorist Town</h2>
      <div class="row">
        <div class="three columns">
        <h3>Your Rank</h3>
        </div>
        <div class="nine columns text-align-right">
          <select id="rank-type-list">
            <option value="kills" selected>Total Kills</option>
            <option value="deaths">Total Deaths</option>
            <option value="headshots">Total Headshots</option>
            <option value="score">Total Score</option>
            <option value="maxfrags">Most Score in a Game</option>

            <option value="playtime">Total Playtime</option>

            <option value="roundsplayed">Rounds Played</option>
            <option value="innocent">Total Innocent Rounds</option>
            <option value="traitor">Total Traitor Rounds</option>
            <option value="detective">Total Detective Rounds</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="three columns" id="user-rank-section">

        </div>
        <div class="nine columns">
          <div class="row margin-20-bottom" id="player-ranks-row1">
            <?php getPlayerRanks(0, "ttt_stats", "kills"); ?>
          </div>
          <div class="row margin-20-bottom" id="player-ranks-row2">
            <?php getPlayerRanks(1, "ttt_stats", "kills"); ?>
          </div>
          <div class="row margin-20-bottom" id="player-ranks-row3">
            <?php getPlayerRanks(2, "ttt_stats", "kills"); ?>
          </div>
          <div class="row" id="player-ranks-row4">
            <?php getPlayerRanks(3, "ttt_stats", "kills"); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<section id="footer-section">
  <footer class="container">
    <div class="row padding-10-top">
      <div class="one-third column">
        <h3>Navigation</h3>
        <ul>
          <a href="index.php"><li>Home</li></a>
          <a href="forums/"><li>Forums</li></a>
          <a href="servers.php"><li>Servers</li></a>
          <a href="donate.php"><li>Donate</li></a>
          <a href="account.php"><li>Account</li></a>
        </ul>
      </div>
      <div class="one-third column">
        <h3>Server Status</h3>
        <div id="server-list-footer">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="twelve colums center-text sub-text-light margin-20-top margin-10-bottom"> &copy; GwServers 2015</div>
    </div>
  </footer>
</section>

<!-- JS
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="js/parallax.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
</body>
</html>
