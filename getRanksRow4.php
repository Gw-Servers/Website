<?php
  error_reporting(E_ALL);
  require 'php/config.php';
  $rankType = $_POST['rankType'];
  $con = mysqli_connect($gs_hostname, $gs_username, $gs_password, $gs_database);
  $escapedRank = mysqli_real_escape_string($con, $_POST["rankType"]);
  mysql_close();

  function convertSteamIdToCommunityId($steamId) {
    if($steamId == 'STEAM_ID_LAN' || $steamId == 'BOT') {
      throw new SteamCondenserException("Cannot convert SteamID \"$steamId\" to a community ID.");
    }
    if (preg_match('/^STEAM_[0-1]:[0-1]:[0-9]+$/', $steamId)) {
      $steamId = explode(':', substr($steamId, 8));
      $steamId = $steamId[0] + $steamId[1] * 2 + 1197960265728;
      return '7656' . $steamId;
    } elseif (preg_match('/^\[U:[0-1]:[0-9]+\]$/', $steamId)) {
        $steamId = explode(':', substr($steamId, 3, strlen($steamId) - 1));
        $steamId = $steamId[0] + $steamId[1] + 1197960265727;
        return '7656' . $steamId;
      } else {
          throw new SteamCondenserException("SteamID \"$steamId\" doesn't have the correct format.");
        }
    }

  try{
    $DBH = new PDO("mysql:host=$gs_hostname;dbname=$gs_database", $gs_username, $gs_password);
    $STH = $DBH->prepare("SELECT @rownum:=@rownum+1 'rank', p.* from ttt_stats p, (SELECT @rownum:=0) r order by $escapedRank desc limit 12,4");
    $STH->execute();
    $STH->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($STH->fetchAll() as $playerRankFetch_fourthRow) {
      $playerRank_score = $playerRankFetch_fourthRow[$rankType];
      $playerRank_place = $playerRankFetch_fourthRow['rank'] + 12;
      $playerRank_Username = $playerRankFetch_fourthRow['nickname'];
      $playerRank_SteamID = $playerRankFetch_fourthRow['steamid'];
      $player_com_id = convertSteamIdToCommunityId($playerRank_SteamID);

      if ($rankType == "playtime") {
        $playerRank_score = floor(($playerRank_score/60)/60);
        $playerRank_score = number_format($playerRank_score);
        $playerRank_score = $playerRank_score." <span class=\"sub-text-light\">Hours</span>";
      }else{
        $playerRank_score = number_format($playerRank_score);
      }

      $playerURL = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=B1FD258B304DAC2C0893367EB5359E29&steamids=".$player_com_id;

      //  Initiate curl
      $ch = curl_init();
      // Disable SSL verification
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      // Will return the response, if false it print the response
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      // Set the url
      curl_setopt($ch, CURLOPT_URL,$playerURL);
      // Execute
      $playerJSON=curl_exec($ch);
      // Closing
      curl_close($ch);

      $infoObj = json_decode($playerJSON, true);
      $playerObj = $infoObj['response']['players'][0];

      $playerAvatar = $playerObj['avatarfull'];
      $playerUsername = $playerObj['personaname'];

      switch ($playerRank_place) {
        case '1':
          $rankColour = "rank-1st";
        break;
        case '2':
          $rankColour = "rank-2nd";
        break;
        case '3':
          $rankColour = "rank-3rd";
        break;
              
        default:
          $rankColour = "rank-nth";
        break;
      }

      echo "
        <div class=\"three columns\">
          <div class=\"row player-rank-box\">
            <div class=\"one-half column\">
              <img src=\"".$playerAvatar."\" class=\"responsive-img profile-img hide-on-break\" height=\"75px\">
            </div>
            <div class=\"one-half column\" id=\"player-rank\">
              <h4 class=\"".$rankColour." no-margin\">".$playerRank_place."</h4>
            </div>
          </div>
          <div class=\"row\" id=\"player-score\">
            <div class=\"player-score-arrow-down-white\"></div>
            <span class=\"sub-text-light\">".$playerRank_Username."</span><br/>
            ".$playerRank_score."
          </div>
        </div>
        ";
    }

  }catch (PDOException $e) {
    echo "Something went tits up.";
    file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
  }

    # ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// $con = mysqli_connect($gs_hostname, $gs_username, $gs_password, $gs_database);

	// $playerRankQuery_fourthRow = mysqli_query($con,"SELECT @rownum:=@rownum+1 'rank', p.* from ttt_stats p, (SELECT @rownum:=0) r order by $rankType desc limit 0,4");
 //  while($playerRankFetch_fourthRow = $db->fetch_array($playerRankQuery_fourthRow)){
 //    $playerRank_score = $playerRankFetch_fourthRow[$rankType];
 //    $playerRank_place = $playerRankFetch_fourthRow['rank'];
 //    $playerRank_Username = $playerRankFetch_fourthRow['nickname'];
 //    $playerRank_SteamID = $playerRankFetch_fourthRow['steamid'];
 //    $player_com_id = convertSteamIdToCommunityId($playerRank_SteamID);

 //    if ($rankType == "playtime") {
 //      $playerRank_score = floor(($playerRank_score/60)/60);
 //      $playerRank_score = number_format($playerRank_score);
 //      $playerRank_score = $playerRank_score." <span class=\"sub-text-light\">Hours</span>";
 //    }else{
 //      $playerRank_score = number_format($playerRank_score);
 //    }

 //    $playerURL = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=B1FD258B304DAC2C0893367EB5359E29&steamids=".$player_com_id;

 //    //  Initiate curl
 //    $ch = curl_init();
 //    // Disable SSL verification
 //    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 //    // Will return the response, if false it print the response
 //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 //    // Set the url
 //    curl_setopt($ch, CURLOPT_URL,$playerURL);
 //    // Execute
 //    $playerJSON=curl_exec($ch);
 //    // Closing
 //    curl_close($ch);

 //    $infoObj = json_decode($playerJSON, true);
 //    $playerObj = $infoObj['response']['players'][0];

 //    $playerAvatar = $playerObj['avatarfull'];
 //    $playerUsername = $playerObj['personaname'];

 //    switch ($playerRank_place) {
 //      case '1':
 //        $rankColour = "rank-1st";
 //      break;
 //      case '2':
 //        $rankColour = "rank-2nd";
 //      break;
 //      case '3':
 //        $rankColour = "rank-3rd";
 //      break;
            
 //      default:
 //        $rankColour = "rank-nth";
 //      break;
 //    }

 //          echo "
 //            <div class=\"three columns\">
 //              <div class=\"row player-rank-box\">
 //                <div class=\"one-half column\">
 //                  <img src=\"".$playerAvatar."\" class=\"responsive-img profile-img hide-on-break\" height=\"75px\">
 //                </div>
 //                <div class=\"one-half column\" id=\"player-rank\">
 //                  <h4 class=\"".$rankColour." no-margin\">".$playerRank_place."</h4>
 //                </div>
 //              </div>
 //              <div class=\"row\" id=\"player-score\">
 //                <div class=\"player-score-arrow-down-white\"></div>
 //                <span class=\"sub-text-light\">".$playerRank_Username."</span><br/>
 //                ".$playerRank_score."
 //              </div>
 //            </div>
 //            ";
 //        }
?>