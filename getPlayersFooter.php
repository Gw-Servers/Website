<?php
require_once 'php/lib/steam-condenser.php';

error_reporting(0);

//$servers = [$server1 = ['Trouble in Terrorist Town #1','185.38.148.137', 27065]];

$servers = array(
	array('Trouble in Terrorist Town #1','185.38.148.137', 27065)
	);

$serverCount = count($servers);
SteamSocket::setTimeout(2000);//in ms

	for ($i=0; $i < $serverCount; $i++) {
		try {
			$actualServer = new SourceServer($servers[$i][1], $servers[$i][2]);
			$actualServer->initialize();
		  $playerCount = $actualServer->getPlayers();
		  $serverInfo = $actualServer->getServerInfo();
		  $maxPlayers = $serverInfo['maxPlayers'];
		  $serverName = $serverInfo['serverName'];
		  $totalCount = count($playerCount);
		  $barWidth = ($totalCount/$maxPlayers)*100;
		  $barColour;
		  $textColour;

		  $fullIP = $servers[$i][1].":".$servers[$i][2];

		  if ($totalCount == 0) {
		  	$status = "server-status-down";
		  }
		  if ($totalCount > 0) {
		  	$status = "server-status-good";
		  }
		  if ($totalCount >= $maxPlayers) {
		  	$status = "server-status-full";
		  }

		  echo "<div class=\"server\">
      	".$servers[$i][0]."
      	<div class=\"".$status."\">".$totalCount."/".$maxPlayers."</div>
    	</div>";


		} catch (Exception $e) {
			//echo "Sever Down!";
			//echo 'Caught exception: ',  $e->getMessage(), "\n";

			echo "<div class=\"server\">
      	<span class=\"sub-text-light\">".$servers[$i][0]."</span>
      	<div class=\"server-status-down\">Offline!</div>
    	</div>";
		}
		
	}


?>