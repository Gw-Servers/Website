<?php
		require 'php/lib/jbbcode/Parser.php';

		//Connection info:
		$host = "localhost";
		$dbname = "rozbarok_mybbforum";
		$user = "rozbarok_mat";
		$pass = "lD;Z2[H1Q6JQ";

		$parser = new JBBCode\Parser();
		$parser->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());

		$builder = new JBBCode\CodeDefinitionBuilder('quote', '<div class="bbcode-quote">{param}</div>');
		$parser->addCodeDefinition($builder->build());
		$builder = new JBBCode\CodeDefinitionBuilder('size', '<h6>{param}</h6>');
		$builder->setUseOption(true)->setOptionValidator(new \JBBCode\validators\CssColorValidator());
		$parser->addCodeDefinition($builder->build());
		$builder = new JBBCode\CodeDefinitionBuilder('spoiler', '<div class="bbcode-quote">{param}</div>');
		$parser->addCodeDefinition($builder->build());
		$builder = new JBBCode\CodeDefinitionBuilder('img', '<img src="{param}" class="responsive-img">');
		$parser->addCodeDefinition($builder->build());

		try {
			$DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
			$STH = $DBH->prepare("SELECT * FROM mybb_threads WHERE fid = '2' ORDER BY dateline DESC LIMIT 0, 5");
			$STH->execute();

			$STH->setFetchMode(PDO::FETCH_ASSOC);
			foreach ($STH->fetchAll() as $getAnnouncementsFetch) {
				//Get thread info
				$author = $getAnnouncementsFetch['username'];
				$subject = $getAnnouncementsFetch['subject'];
				$replies = $getAnnouncementsFetch['replies'];
				$date = date("F j, Y", $getAnnouncementsFetch['dateline']);
				$threadID = $getAnnouncementsFetch['tid'];
				$uid = $getAnnouncementsFetch['uid'];

				//manipulate title
				if (strlen($subject) >= 28) {
					$subject = substr($subject, 0,28);
					$subject .= "...";
				}

				$STH = $DBH->prepare("SELECT * FROM mybb_posts WHERE tid = :threadID AND replyto = '0'");
				$STH->bindParam(':threadID', $threadID);
				$STH->execute();
				while ($getPostFetch = $STH->fetch()) {
					//Get post
					$actualPost = $getPostFetch['message'];

					//manipulate post
					$actualPost = str_replace("\r\n","<br/>",$actualPost);
					$actualPost = str_replace("</h6><br/>","</h6>",$actualPost);
					$actualPost = str_replace("<div class=\"bbcode-quote\"><br/>","<div class=\"bbcode-quote\">",$actualPost);
					if (strlen($actualPost) >= 500) {
						$actualPost = substr($actualPost, 0,500);
					}
					$parser->parse($actualPost);
					$actualPost = $parser->getAsBBcode();
					$actualPost = $parser->getAsHtml();
					if (strlen($actualPost) >= 400) {
						$actualPost .= "<a href=\"../forums/showthread.php?tid=".$threadID."\"> ... Read More</a>";
					}
				}

				$STH = $DBH->prepare("SELECT * FROM mybb_users WHERE uid = :uid");
				$STH->bindParam(':uid', $uid);
				$STH->execute();
				while ($authorAvatarfetch = $STH->fetch()) {
					$userAvatar = $authorAvatarfetch['avatar'];
				}

				$getAnnouncements .= "
				<div class=\"row content-box margin-20-bottom\">
          <div class=\"row\">
            <div class=\"two columns\">
              <div class=\"center-text\">
                <img src=\"../forums/".$userAvatar."\" class=\"responsive-img profile-img\">
              </div>
            </div>
            <div class=\"ten columns\">
              <h2 class=\"no-margin\"><a href=\"../forums/showthread.php?tid=".$threadID."\">".$subject."</a></h2>
              <span class=\"sub-text-dark\">".$date."</span> - <span class=\"colour-owner\">".$author."</span><br/>
              <span class=\"sub-text-dark\">".$replies." Replies</span>
            </div>
          </div>
          <div class=\"row\">
            ".$actualPost."
          </div>
        </div>
			";

			}

		} catch (PDOException $e) {
			echo "Something went tits up.";
			file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
		}

?>