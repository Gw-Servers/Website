<?php
  error_reporting(E_ALL);
  require 'php/config.php';
  require 'randomHero.php';
  require 'getUserInfo.php';
  require 'getRecentDonations.php';
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Get wrecked! Servers - Donate</title>
    <meta name="description" content="Get Wrecked Servers. Premium Garry's Mod servers and community!">
    <meta name="author" content="GwServers">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/jquery.countdown.css">
    <link rel="stylesheet" href="css/main.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="img/favicon.png">

</head>
<body>

<section id="top-bar">
  <header class="container">
    <div class="row">
      <div class="three columns" id="logo">
        <a href="index.php"><img src="img/logo.png" height="50px"></a>
      </div>
      <div class="nine columns" id="navbar">
        <input type="checkbox" id="show-menu" role="button" value="Open/Close Menu" />
        <label for="show-menu" class="show-menu">Menu</label>
        <ul id="main-menu">
          <li><a href="forums/">Forums</a></li>
          <li><a>Servers</a>
            <ul>
              <li><a href="trouble-in-terrorist-town-1.php">Trouble in Terrorist Town #1</a></li>
              <li><a href="leaderboards.php">Leaderboards</a></li>
            </ul>
          </li>
          <li><a href="donate.php">Donate</a></li>
          <?php
            if ($mybb->user['uid']) {
              echo "
          <li><a>Account<img src=\"../forums/".$mybb->user['avatar']."\"class=\"responsive-img hide-on-break\" id=\"nav-user-img\" height=\"40px\"></a>
            <ul>
              <li><a href=\"../forums/usercp.php\">View Profile</a></li>
              <li><a href=\"../forums/usercp.php\">Settings</a></li>
              <li><a href=\"../forums/member.php?action=logout&amp;logoutkey={$mybb->user['logoutkey']}\">Logout</a></li>
            </ul>
          </li>
              ";
            }
            else{
              echo "<li><a href=\"../forums/member.php?action=login\">Login</a></li>";
            }
          ?>
        </ul>
      </div>
    </div>
  </header>
</section>

<section id="hero" data-parallax="scroll" data-image-src="<?php echo $imageToDisplay; ?>" data-speed="0.6" data-position="0px <?php echo $imagePosition;?>">
  <div class="container">

    <div class="row">
      <div class="twelve columns center-text <?php echo $heroTextColour; ?>">
        <h1>Donations</h1>
        <h4 class="margin-20-bottom">Your donations keep us alive!</h4>
      </div>
    </div>

    <div class="row" id="server-button-row">

      <div class="one-third column server-button">
      <a href="#why-donate-modal">
        <img src="img/icons/coin_pound.png" alt="Connect icon" width="32px">
        Why Donate?
        </a>
      </div>

      <!-- <div class="one-third column">
        <div class="donation-bar center-text">
          <div class="donation-bar-inner" style="width:60%;"></div>
        </div>
        <div class="donation-bar-text"><strong>Monthly Target: 60%</strong></div>
      </div> -->

      <div class="one-third column server-button offset-by-one-third">
        <a href="#terms-modal">
          <img src="img/icons/accepted_document.png" alt="Connect icon" width="32px">
          Terms &amp; Conditions
        </a>
      </div>

  </div>

</section>

<section id="main-content">
      
      <?php 
      if (isset($_GET["convertError"])) {
        $errorMessage = "<div class='center-text'><h2>Oops!</h2></div>
             <p>Looks like something went wrong while you were trying to convert tokens.</p>
             <p>Make sure that you:</p>
             <ul>
               <li>Entered in numbers only</li>
               <li>You have enough tokens to convert</li>
               <li>You entered in at least 1 token.</li>
             </ul>
             <p>If problems persist, please contact Mike or Mat.</p>";

        echo "
        <div class='container padding-20-top'>
        <div class='row'>
          <div class='alert-error'>
          ".$errorMessage."
          </div>
        </div>
        ";
      }
      if (isset($_GET['convertSuccess'])) {
        $convertMessage = "<div class='center-text'><h2>Tokens converted!</h2></div>";
        echo "
        <div class='container padding-20-top'>
        <div class='row'>
          <div class='alert-success'>
          ".$convertMessage."
          </div>
        </div>
        ";
      }
      if (isset($_GET['purchaseSuccess'])) {
        $purchaseMessage = "<div class='center-text'><h2>Rank Purchased!</h2></div>";
        echo "
        <div class='container padding-20-top'>
        <div class='row'>
          <div class='alert-success'>
          ".$purchaseMessage."
          </div>
        </div>
        ";
      }
      if (isset($_GET['purchaseError'])) {
        $purchaseMessage = "<div class='center-text'><h2>You have too few tokens!</h2></div>";
        echo "
        <div class='container padding-20-top'>
        <div class='row'>
          <div class='alert-error'>
          ".$purchaseMessage."
          </div>
        </div>
        ";
      }
      if (isset($_GET['loginError'])) {
        $purchaseMessage = "<div class='center-text'><h2>Please login before you purchase a rank!</h2></div>";
        echo "
        <div class='container padding-20-top'>
        <div class='row'>
          <div class='alert-error'>
          ".$purchaseMessage."
          </div>
        </div>
        ";
      }
      if (isset($_GET['connectError'])) {
        $purchaseMessage = "<div class='center-text'><h3>Please play on the server at least once before buying a rank.</h3></div>";
        echo "
        <div class='container padding-20-top'>
        <div class='row'>
          <div class='alert-error'>
          ".$purchaseMessage."
          </div>
        </div>
        ";
      }


      ?>

    
      <?php
      $success = $_GET["success"];
      $donateMessage = "
      <div class='center-text'><h2>Thank you!</h2></div>
      <p>Thank you very much for donating!</p>
      <p>You will soon recieve an email regarding the transaction from ourselves and you will recieve your tokens once the paypal payment is complete. <strong>This email will be sent to the email you used in the paypal transaction</strong></p>
      <p>If you have not recieved an email or your tokens, please contact one of the owners as soon as possible.</p>
      ";
      if (isset($success)) {
        echo "
        <div class='container padding-20-top'>
        <div class='row'>
          <div class='alert-success'>
          ".$donateMessage."
          </div>
        </div>
        ";
      }
      if (!isset($success) && !isset($_GET["convertError"]) && !isset($_GET["convertSuccess"]) && !isset($_GET['purchaseSuccess']) && !isset($_GET['purchaseError']) && !isset($_GET['loginError']) && !isset($_GET['connectError']) ){
        echo "<div class='container'>";
      }
      ?>

    <div class="row">
      <div class="two-thirds column margin-20-top margin-20-bottom">


      <?php if ($mybb->user['uid'] && $mybb->user['uniqueid']) { ?>
      <div class="row content-box margin-20-bottom">
        <?php 
          $uniqueID = $mybb->user['uniqueid'];
        ?>
        <div class="row">
          <form action="https://www.paypal.com/cgi-bin/webscr" method="post" class="no-margin">
            <div class="width-20pc token-purchase" style="float:left;">Tokens:</div>
              <div class="width-40pc token-amount" style="float:left;">
                <input type="number" min="0" value="100" class="u-full-width" id="tokens">
              </div>
              <div class="width-25pc token-button" style="float:left;">
                £<span id="token-cost">0.00</span>
              </div>
                <input type="hidden" name="cmd" value="_xclick" class="no-margin">
                <input type="hidden" name="business" value="getwreckedservers@gmail.com" class="no-margin">
                <input type="hidden" name="currency_code" value="GBP" class="no-margin">
                <input type="hidden" name="notify_url" value="http://gwservers.co.uk/dev2/paypal_ipn.php" class="no-margin">
                <input type="hidden" name="item_name" value="Tokens" class="no-margin">
                <input type="hidden" name="amount" value="0.10" class="no-margin">
                <input type="hidden" name="return" value="http://gwservers.co.uk/dev2/donate.php?success" class="no-margin">
                <input type="hidden" name="quantity" value="0" id="paypal-quant" class="no-margin">
                <input type="hidden" name="custom" value="<?php $uniqueID; ?>" class="no-margin">
                <input type="image" src="img/paypal-buy.png" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!" class="no-margin">
            <div class="clear"></div>
          </form>
        </div>
      </div>

      <?php } elseif (!isset($mybb->user['uniqueid']) && $mybb->user['uid']) { ?>
        <div class="row center-text content-box margin-20-bottom">
          <h4 class="no-margin">Please connect your Steam Account to your forum account to purchase or convert Tokens!</h4>
          <a href="#" class="button button-primary">Connect!</a>
        </div>
      <?php } elseif ($mybb->user['uniqueid'] == "0" && $mybb->user['uid']) { ?>
        <div class="row center-text content-box margin-20-bottom">
          <h4 class="no-margin">Please connect your Steam Account to your forum account to purchase or convert Tokens!</h4>
          <a href="#" class="button button-primary">Connect!</a>
        </div>
      <?php } else{ ?>

        <div class="row center-text content-box margin-20-bottom">
          <h4 class="no-margin">Please login to purchase or convert Tokens!</h4>
          <a href="../forums/member.php?action=login" class="button button-primary">Log In!</a>
          <a href="../forums/member.php?action=register" class="button button-primary">Register!</a>
        </div>

        <?php } ?>

      <div class="row content-box">

        <div class="row margin-10-top">

          <div class="six columns">
            <form action="purchaseRank.php" method="POST" class="no-margin">
              <input type="hidden" name="uniqueid" value="<?php echo $uniqueID; ?>">
              <input type="hidden" value="monarch" name="rankName" class="no-margin">
              <div class="row monarch-title">
                <h5 class="margin-5-bottom padding-5-top">Monarch</h5>
              </div>
              <div class="row best-value">
                <img src="img/icons/coin_pound.png" width="26px" class="vertical-align-middle">Best Value!
              </div>
              <div class="row donate-option-content">
                <ul>
                  <li>6,000 Bob <a href="#bob-modal"><img src="img/icons/help.png" width="20px" class="vertical-align-middle"></a></li>
                  <li>Monarch Title</li>
                  <li><span class="colour-monarch">Coloured <strong>Username</strong> and <strong>Title</strong></span></li>
                </ul>
                <div class="center-text">
                  <button type="submit" class="button-primary">250 Tokens</button>
                </div>
              </div>
            </form>
          </div>


          <div class="six columns">
            <form action="purchaseRank.php" method="POST" class="no-margin">
              <input type="hidden" name="uniqueid" value="<?php echo $uniqueID; ?>">
              <input type="hidden" value="viscount" name="rankName" class="no-margin">
              <div class="row viscount-title">
                <h5 class="margin-5-bottom padding-5-top">Viscount</h5>
              </div>
              <div class="row best-value">
                <img src="img/icons/shape_star.png" width="26px" class="vertical-align-middle">Most Popular!
              </div>
              <div class="row donate-option-content">
                <ul>
                  <li>
                    4,000 Bob <a href="#bob-modal"><img src="img/icons/help.png" width="20px" class="vertical-align-middle"></a>
                  </li>
                  <li>Viscount Title</li>
                  <li><span class="colour-viscount">Coloured <strong>Username</strong> and <strong>Title</strong></span></li>
                </ul>
                <div class="center-text">
                  <button type="submit" class="button-primary">175 Tokens</button>
                </div>
              </div>
            </form>
          </div>

        </div>

        <div class="row margin-20-top margin-20-bottom">

          <div class="six columns">
            <form action="purchaseRank.php" method="POST" class="no-margin">
              <input type="hidden" name="uniqueid" value="<?php echo $uniqueID; ?>">
              <input type="hidden" value="baron" name="rankName" class="no-margin">
              <div class="row baron-title">
                <h5 class="margin-5-bottom padding-5-top">Baron</h5>
              </div>
              <div class="row donate-option-content">
                <ul>
                  <li>
                    2,500 Bob <a href="#bob-modal"><img src="img/icons/help.png" width="20px" class="vertical-align-middle"></a>
                  </li>
                  <li>Baron Title</li>
                  <li><span class="colour-baron">Coloured <strong>Username</strong> and <strong>Title</strong></span></li>
                </ul>
                <div class="center-text">
                  <button type="submit" class="button-primary">100 Tokens</button>
                </div>
              </div>
            </form>
          </div>

          <div class="six columns">
            <form action="purchaseRank.php" method="POST" class="no-margin">
              <input type="hidden" name="uniqueid" value="<?php echo $uniqueID; ?>">
              <input type="hidden" value="knight" name="rankName" class="no-margin">
              <div class="row knight-title">
                <h5 class="margin-5-bottom padding-5-top">Knight</h5>
              </div>
              <div class="row donate-option-content">
                <ul>
                  <li>
                    1,000 Bob <a href="#bob-modal"><img src="img/icons/help.png" width="20px" class="vertical-align-middle"></a>
                  </li>
                  <li>Knight Title</li>
                  <li><span class="colour-knight">Coloured <strong>Username</strong> and <strong>Title</strong></span></li>
                </ul>
                <div class="center-text">
                  <button type="submit" class="button-primary">50 Tokens</button>
                </div>
              </div>
            </form>
          </div>
          
        </div>

        
      </div>

      <div class="row content-box margin-20-top">
        <?php if ($mybb->user['uid']) { ?>

        <div class="row">
          <form action="convertTokens.php" method="POST" class="no-margin">
            <div class="width-20pc token-purchase no-margin" style="float:left;">Tokens:</div>
              <div class="width-40pc bob-amount no-margin" style="float:left;">
                <input type="number" min="0" value="1" class="u-full-width no-margin" id="bobs" name="bobs">
              </div>
              <div class="width-25pc bob-button no-margin" style="float:left;">
                Bob
              </div>
              <input type="hidden" name="uid" value="<?php echo $mybb->user['uid']; ?>" class="no-margin">
              <div class="width-15pc" style="float:right;">
                <input type="image" src="img/token-convert.png" type="submit" value="" class="no-margin">
              </div>
            <div class="clear"></div>
          </form>
        </div>

        <?php } ?>
      </div>      

      </div>
      <div class="one-third column margin-20-top">
        <div class="row content-box margin-20-bottom">
          <?php echo $getUserInfo; ?>
        </div>
        <div class="row content-box margin-20-bottom">
          <h5 class="no-margin">Recent Donations</h5>
          <table class="u-full-width no-margin">
            <tbody>
              <?php echo $donation; ?>
            </tbody>
          </table>
        </div>
        <div class="row center-text margin-20-bottom">
          <a href="#terms-modal">Terms &amp; conditions</a>
        </div>
      </div>
    </div>

  </div>
</section>

<section id="footer-section">
  <footer class="container">
    <div class="row padding-10-top">
      <div class="one-third column">
        <h3>Navigation</h3>
        <ul>
          <a href="index.php"><li>Home</li></a>
          <a href="forums/"><li>Forums</li></a>
          <a href="servers.php"><li>Servers</li></a>
          <a href="donate.php"><li>Donate</li></a>
          <?php
            if ($mybb->user['uid']) {
              echo '<a href="../forums/usercp.php"><li>Account</li></a>';
              echo "<a href=\"../forums/member.php?action=logout&amp;logoutkey={$mybb->user['logoutkey']}\"><li>Logout</li></a>";
            }else {
              echo '<a href="../forums/member.php?action=login"><li>Login / Register</li></a>';
            };
          ?>
        </ul>
      </div>
      <div class="one-third column">
        <h3>Server Status</h3>
        <div id="server-list-footer">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="twelve colums center-text sub-text-light margin-20-top margin-10-bottom"> &copy; GwServers 2015 - <a href="#cookie-modal">Cookie Policy</a></div>
    </div>
  </footer>
</section>

<!-- Modals
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="why-donate-modal" class="modalDialog">
  <div class="modal-inner">
    <a href="#close" title="Close" class="close">X</a>
    <div class="modal-scroll">
      <h3>Why Donate?</h3>
      <p>Your donations are the life blood of our servers. we depend on donations to keep the servers up and running and constantly updated to bring you and the rest of our players the best experience we can offer.</p>
      <p>There may be times when we exceed our monthly costs in terms of donations, in this case we will pool the remainder into a fund for expanding and upgrading our servers.</p>
      <p>For your donations, we will reward you with our <strong>Premium</strong> currency, Tokens, which you may choose to spend on what ever premium item or rank you decide on.</p>
      <p>If there is an item, or service we can offer as a reward that we currently do not offer, please post on the forums</p>
      <br/>
      <div class="modal-footer">
        <div style="float: left;"><a href="#tokens-modal">What Are Tokens?</a> - </div>
        <div style="float: left;"> <a href="#bob-modal"> What is Bob?</a></div>
        <div style="float: right;"><a href="#terms-modal">Terms &amp; Conditions</a></div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>

<div id="bob-modal" class="modalDialog">
  <div class="modal-inner">
    <a href="#close" title="Close" class="close">X</a>
    <div class="modal-scroll">
      <h3>What is Bob</h3>
      <p>Bob in our in game currency. Bob can be used to purchase any items in our store such as player skins.</p>
      <p>Bob can be earned by playing on any of our servers, through completing gamemode specific tasks such as killing a Traitor. Bob can also be earned over time at a rate of 10 bob, every 5 minutes of active play.</p>
      <p>for those that wish for a quick boost in Bob, you can convert tokens into bob at a rate of 100 bob per token.</p>
      <br/>
      <div class="modal-footer">
        <div style="float: left;"><a href="#tokens-modal">What Are Tokens?</a> - </div>
        <div style="float: left;"> <a href="#why-donate-modal"> Why Donate?</a></div>
        <div style="float: right;"><a href="#terms-modal">Terms &amp; Conditions</a></div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>

<div id="tokens-modal" class="modalDialog">
  <div class="modal-inner">
    <a href="#close" title="Close" class="close">X</a>
    <div class="modal-scroll">
      <h3>What are Tokens?</h3>
      <p>Tokens are our <strong>Premium</strong> in game currency. Tokens can be used to purchase items in our donator store.</p>
      <p>Tokens are currently priced at £0.10 per token. This may change in the future, and we may offer discounted prices for events.</p>
      <p>Tokens will also be featured as event prizes, so look out.</p>
      <div class="modal-footer">
        <div style="float: left;"><a href="#bob-modal">What is Bob?</a> - </div>
        <div style="float: left;"> <a href="#why-donate-modal"> Why Donate?</a></div>
        <div style="float: right;"><a href="#terms-modal">Terms &amp; Conditions</a></div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>

<div id="terms-modal" class="modalDialog">
  <div class="modal-inner">
    <a href="#close" title="Close" class="close">X</a>
    <div class="modal-scroll">
      <h3>Terms &amp; Conditions</h3>
      <p>By donating, you agree to all terms and conditions stated below.</p>
      <h4>Donation Rewards</h4>
        <p>Donations have been set up to allow you to support our community. Please remember that all rewards offered for donations are just that, rewards. While we wish to thank you for your contributions, please remember that you are not entitled to gain something in return. If there is something you would like to be offered as a reward, pleaase suggest it in the forums where the community and staff can discuss the suggestion.</p>

        <h4>Refund Policy</h4> 
        <p>Get wrecked! will not provide refunds under any circumstances.</p>
        <ul>
          <li>If banned, any donations you have made <strong>Will Not</strong> be refunded.</li>
          <li>You <strong>Will Not</strong> Be refunded if you later decide that you do not want your donation purchase</li>
          <li>If you have not recieved your donation rewards, but cannot prove that you have donated, you <strong>Will Not</strong> revieve a refund.</li>
        </ul>

        <h4>Donation Exchange</h4>
        <p>Here at Get wrecked! we have set up out donation system to allow you to have complete control on what rewards you gain from donating. If you decide that you wish to exchange your rewards for other rewards, we may grant this wish under the discretion of the owners. Please contact an owner or admin for more information.</p>

        <h4>Disclaimer of warranties</h4>
        <p>YOU UNDERSTAND AND AGREE THAT YOUR USE OF THIS WEB SITE AND ANY SERVICES OR CONTENT PROVIDED (THE SERVICE) IS MADE AVAILABLE AND PROVIDED TO YOU AT YOUR OWN RISK. IT IS PROVIDED TO YOU AS IS AND WE EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, IMPLIED OR EXPRESS, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. WE MAKE NO WARRANTY, IMPLIED OR EXPRESS, THAT ANY PART OF THE SERVICE WILL BE UNINTERRUPTED, ERROR-FREE, TIMELY, SECURE, ACCURATE, RELIABLE, OF ANY QUALITY, NOR THAT ANY CONTENT IS SAFE IN ANY MANNER FOR DOWNLOAD. YOU UNDERSTAND AND AGREE THAT NEITHER US NOR ANY PARTICIPANT IN THE SERVICE PROVIDES PROFESSIONAL ADVICE OF ANY KIND AND THAT USE OF SUCH ADVICE OR ANY OTHER INFORMATION IS SOLELY AT YOUR OWN RISK AND WITHOUT OUR LIABILITY OF ANY KIND. Some jurisdictions may not allow disclaimers of implied warranties and the above disclaimer may not apply to you only as it relates to implied warranties.</p>

        <h4>Limitation of liabilities</h4> 
        <p>You agree to give up all legal rights towards Get wrecked! when donating.</p>

      <span class="sub-text-dark"><p>If you have any questions regarding this disclaimer, please contact one of the owners.</p></span>
      <div class="modal-footer">
        <div style="float: left;"><a href="#tokens-modal">What Are Tokens?</a> - </div>
        <div style="float: left;"> <a href="#bob-modal"> What is Bob?</a></div>
        <div style="float: right;"><a href="#why-donate-modal">Why Donate?</a></div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>

<div id="cookie-modal" class="modalDialog">
	<div class="modal-inner">
		<a href="#close" title="Close" class="close">X</a>
    <div class="modal-scroll">
      <h3>Cookie Policy</h3>
      <p>By continuing use of this website, you are agreeing to the use of cookies on this domain and all subdomains of this website.</p>
  		<h4>What Are Cookies</h4>
      <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or 'break' certain elements of the sites functionality.</p>
      <p>For more general information on cookies see the <a href="https://en.wikipedia.org/wiki/HTTP_cookie">Wikipedia</a> article on HTTP Cookies...</p>
      <h4>How We Use Cookies</h4>
      <p>We use cookies for a variety of reasons detailed below. Unfortunately is most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.</p>
      <h4>Disabling Cookies</h4>
      <p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of the this site. Therefore it is recommended that you do not disable cookies.</p>
      <h4>The Cookies We Set</h4>
      <p>If you create an account with us then we will use cookies for the management of the signup process and general administration. These cookies will usually be deleted when you log out however in some cases they may remain afterwards to remember your site preferences when logged out.</p>
      <p>We use cookies when you are logged in so that we can remember this fact. This prevents you from having to log in every single time you visit a new page. These cookies are typically removed or cleared when you log out to ensure that you can only access restricted features and areas when logged in.</p>
      <p>In order to provide you with a great experience on this site we provide the functionality to set your preferences for how this site runs when you use it. In order to remember your preferences we need to set cookies so that this information can be called whenever you interact with a page is affected by your preferences.</p>
      <h4>Third Party Cookies</h4>
      <p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this site.</p>
      <p>This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.</p>
      <p>For more information on Google Analytics cookies, see the official Google Analytics page.</p>
      <h4>More Information</h4>
      <p>Hopefully that has clarified things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave cookies enabled in case it does interact with one of the features you use on our site. However if you are still looking for more information then you can contact us through one of our preferred contact methods.</p>
      <p>Email: cookies@gwservers.co.uk</p>
    </div>
	</div>
</div>

<!-- JS
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="js/parallax.min.js"></script>
  <script type="text/javascript" src="js/main.js" async></script>
</body>
</html>
