<?php
  error_reporting(E_ALL);
  require 'php/config.php';
  $server = "ttt1";
  $serverIP = "185.38.148.137:27065";
  require 'getServerInfo.php';
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Get wrecked! Servers - Trouble in Terrorist Town #1</title>
    <meta name="description" content="Get Wrecked Servers. Premium Garry's Mod servers and community!">
    <meta name="author" content="GwServers">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/jquery.countdown.css">
    <link rel="stylesheet" href="css/main.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="img/favicon.png">

</head>
<body>

  <section id="top-bar">
    <header class="container">
      <div class="row">
        <div class="three columns" id="logo">
          <a href="index.php"><img src="img/logo.png" height="50px"></a>
        </div>
        <div class="nine columns" id="navbar">
          <input type="checkbox" id="show-menu" role="button" value="Open/Close Menu" />
          <label for="show-menu" class="show-menu">Menu</label>
          <ul id="main-menu">
            <li><a href="forums/">Forums</a></li>
            <li><a>Servers</a>
              <ul>
                <li><a href="trouble-in-terrorist-town-1.php">Trouble in Terrorist Town #1</a></li>
                <li><a href="leaderboards.php">Leaderboards</a></li>
              </ul>
            </li>
            <li><a href="donate.php">Donate</a></li>
            <?php
              if ($mybb->user['uid']) {
                echo "
            <li><a>Account<img src=\"../forums/".$mybb->user['avatar']."\"class=\"responsive-img hide-on-break\" id=\"nav-user-img\" height=\"40px\"></a>
              <ul>
                <li><a href=\"../forums/usercp.php\">View Profile</a></li>
                <li><a href=\"../forums/usercp.php\">Settings</a></li>
                <li><a href=\"../forums/member.php?action=logout&amp;logoutkey={$mybb->user['logoutkey']}\">Logout</a></li>
              </ul>
            </li>
                ";
              }
              else{
                echo "<li><a href=\"../forums/member.php?action=login\">Login</a></li>";
              }
            ?>
          </ul>
        </div>
      </div>
    </header>
  </section>

<section id="hero" data-parallax="scroll" data-image-src="<?php echo $imageToDisplay_server; ?>" data-speed="0.6" data-position="0px <?php echo $imagePosition;?>">
  <div class="container">
    <div class="row">
      <div class="twelve columns center-text <?php echo $heroTextColour; ?>">
        <h1>Trouble in Terrorist Town #1</h1>
        <h3><?php echo $taglineString; ?></h3>
      </div>
    </div>
    <div class="row" id="server-button-row">
      <div class="one-third column server-button" id="connect-button">
        <a href="steam://connect/<?php echo $serverIP;?>">
          <img src="img/icons/connect.png" alt="Connect icon" width="32px">
          Connect
        </a>
      </div>
      <div class="one-third column server-button" id="favourite-button">
        <a href="steam://connect/<?php echo $serverIP;?>">
          <img src="img/icons/heart.png" alt="favourite icon" width="32px">
          Favourite
        </a>
      </div>
      <div class="one-third column server-button" id="download-button">
        <?php
          if ($mybb->user['uid']) {
            echo '
            <a href="downloads/GwMaps.7z">
              <img src="img/icons/zipped_file.png" alt="Download icon" width="32px">
              Download Maps
            </a>
            ';
          }else {
            echo '
            <a href="../forums/member.php?action=login">
              <img src="img/icons/lock.png" alt="Download icon" width="32px">
              Login to Download Maps
            </a>
            ';
          }
        ?>
      </div>
    </div>
  </div>
</section>

<section id="main-content">
  <div class="container">
    <div class="row margin-20-bottom">

      <div class="two-thirds column content-box margin-20-top">

        <div class="row">
          <div class="two-thirds column">
            <div class="row">
              <h3>Trouble in Terrorist Town</h3>
            </div>
          </div>
          <div class="one-third column">
            <select id="rank-type-list" class="u-full-width">
              <option value="kills" selected>Total Kills</option>
              <option value="deaths">Total Deaths</option>
              <option value="headshots">Total Headshots</option>
              <option value="score">Total Score</option>
              <option value="maxfrags">Most Score in a Game</option>

              <option value="playtime">Total Playtime</option>

              <option value="roundsplayed">Rounds Played</option>
              <option value="innocent">Total Innocent Rounds</option>
              <option value="traitor">Total Traitor Rounds</option>
              <option value="detective">Total Detective Rounds</option>
            </select>
          </div>
        </div>

        <div class="row margin-20-bottom" id="player-ranks">

        </div>
        <div class="row" id="player-ranks2">

        </div>
      </div>

      <div class="one-third column content-box margin-20-top">
        <h4>Current Player List</h4>
        <div class="sub-text-dark center-text">
          <?php
            if (!isset($totalCount)) {
              echo "Server Offline";
            }
            elseif ($totalCount == "0") {
              echo "No Players Online!";
            }else{
              echo $totalCount."/".$maxPlayers." Players";
            }
          ?>
        </div>
        <div style="max-height: 370px; overflow: auto;">
          <table class="u-full-width">
            <?php
              foreach($playerCount as $player) {
                echo "<tr>";
                  echo "<td><td>";
                  echo "<td>{$player->getName()}</td>";
                  echo "<td>{$player->getScore()}</td>";
                echo "</tr>";
              }
            ?>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="footer-section">
  <footer class="container">
    <div class="row padding-10-top">
      <div class="one-third column">
        <h3>Navigation</h3>
        <ul>
          <a href="index.php"><li>Home</li></a>
          <a href="forums/"><li>Forums</li></a>
          <a href="servers.php"><li>Servers</li></a>
          <a href="donate.php"><li>Donate</li></a>
          <?php
            if ($mybb->user['uid']) {
              echo '<a href="../forums/usercp.php"><li>Account</li></a>';
              echo "<a href=\"../forums/member.php?action=logout&amp;logoutkey={$mybb->user['logoutkey']}\"><li>Logout</li></a>";
            }else {
              echo '<a href="../forums/member.php?action=login"><li>Login / Register</li></a>';
            };
          ?>
        </ul>
      </div>
      <div class="one-third column">
        <h3>Server Status</h3>
        <div id="server-list-footer">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="twelve colums center-text sub-text-light margin-20-top margin-10-bottom"> &copy; GwServers 2015 - <a href="#cookie-modal">Cookie Policy</a></div>
    </div>
  </footer>
</section>

<!-- Modals
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="cookie-modal" class="modalDialog">
	<div class="modal-inner">
		<a href="#close" title="Close" class="close">X</a>
    <div class="modal-scroll">
      <h3>Cookie Policy</h3>
      <p>By continuing use of this website, you are agreeing to the use of cookies on this domain and all subdomains of this website.</p>
  		<h4>What Are Cookies</h4>
      <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or 'break' certain elements of the sites functionality.</p>
      <p>For more general information on cookies see the <a href="https://en.wikipedia.org/wiki/HTTP_cookie">Wikipedia</a> article on HTTP Cookies...</p>
      <h4>How We Use Cookies</h4>
      <p>We use cookies for a variety of reasons detailed below. Unfortunately is most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.</p>
      <h4>Disabling Cookies</h4>
      <p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of the this site. Therefore it is recommended that you do not disable cookies.</p>
      <h4>The Cookies We Set</h4>
      <p>If you create an account with us then we will use cookies for the management of the signup process and general administration. These cookies will usually be deleted when you log out however in some cases they may remain afterwards to remember your site preferences when logged out.</p>
      <p>We use cookies when you are logged in so that we can remember this fact. This prevents you from having to log in every single time you visit a new page. These cookies are typically removed or cleared when you log out to ensure that you can only access restricted features and areas when logged in.</p>
      <p>In order to provide you with a great experience on this site we provide the functionality to set your preferences for how this site runs when you use it. In order to remember your preferences we need to set cookies so that this information can be called whenever you interact with a page is affected by your preferences.</p>
      <h4>Third Party Cookies</h4>
      <p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this site.</p>
      <p>This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.</p>
      <p>For more information on Google Analytics cookies, see the official Google Analytics page.</p>
      <h4>More Information</h4>
      <p>Hopefully that has clarified things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave cookies enabled in case it does interact with one of the features you use on our site. However if you are still looking for more information then you can contact us through one of our preferred contact methods.</p>
      <p>Email: cookies@gwservers.co.uk</p>
    </div>
	</div>
</div>

<!-- JS
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="js/parallax.min.js"></script>
  <script type="text/javascript" src="js/main.js" async></script>
  <script type="text/javascript" src="js/getRanksServerTTT.js" async></script>
</body>
</html>
