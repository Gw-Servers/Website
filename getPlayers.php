<?php
require_once 'php/lib/steam-condenser.php';

error_reporting(0);

//$servers = [$server1 = ['Trouble in Terrorist Town #1','185.38.148.137', 27065]];

$servers = array(
	array('Trouble in Terrorist Town #1','185.38.148.137', 27065)
	);


$serverCount = count($servers);
SteamSocket::setTimeout(2000);//in ms

	for ($i=0; $i < $serverCount; $i++) {
		try {
			$actualServer = new SourceServer($servers[$i][1], $servers[$i][2]);
			$actualServer->initialize();
		  $playerCount = $actualServer->getPlayers();
		  $serverInfo = $actualServer->getServerInfo();
		  $maxPlayers = $serverInfo['maxPlayers'];
		  $serverName = $serverInfo['serverName'];
		  $totalCount = count($playerCount);
		  $barWidth = ($totalCount/$maxPlayers)*100;
		  $barColour;
		  $textColour;

		  $fullIP = $servers[$i][1].":".$servers[$i][2];

		  if ($totalCount == 0) {
		  	$barColour = "down-bar";
		  	$textColour = "colour-down";
		  	$barWidth = "1";
		  }
		  if ($totalCount > 0) {
		  	$barColour = "good-bar";
		  	$textColour = "colour-good";
		  }
		  if ($totalCount >= $maxPlayers) {
		  	$barColour = "full-bar";
		  	$textColour = "colour-full";
		  }

		  echo "
		  		<div class=\"row server-stats alive margin-20-bottom margin-10-top\">
            <div class=\"row\">
              <div class=\"center-text\"><h6 class=\"no-margin-bottom\"><strong>".$servers[$i][0]."</strong></h6></div>
            </div>
            <div class=\"row\" style=\"height: 24px\">
              <div class=\"status-bar ".$barColour."\">
                <span style=\"width: ".$barWidth."%\" class=\"center-text\"><span></span></span>
              </div>
              <a href=\"steam://connect/".$servers[$i][1].":".$servers[$i][2]."\"><div class=\"status-bar-text ".$textColour."\"><strong>".$totalCount."/".$maxPlayers."</strong></div></a>
            </div>
          </div>
          ";


		} catch (Exception $e) {
			echo "<div class=\"row server-stats dead margin-20-bottom margin-10-top\">
      <div class=\"row\">
      	<div class=\"center-text\"><h6 class=\"no-margin-bottom sub-text-dark\"><strong>".$servers[$i][0]."</strong></h6></div>
      </div>
      <div class=\"row\" style=\"height: 24px\">
      	<div class=\"status-bar colour-down\">
          <span class=\"center-text\"></span>
        </div>
        <div class=\"status-bar-text sub-text-dark\"><strong>Offline!</strong></div>
      </div>
    </div>";
		}

	}


?>
