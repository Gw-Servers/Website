<?php
error_reporting(0);
require_once 'php/lib/steam-condenser.php';

switch ($server) {
  case 'ttt1':
    try{
      $sourceServer = new SourceServer("5.101.136.122", 27015);
      $sourceServer->initialize();
      $serverInfo = $sourceServer->getServerInfo();
      $playerCount = $sourceServer->getPlayers();
      $totalCount = count($playerCount);
      $status = "Online!";
      if ($totalCount == "0") {
        $taglineString = "No Players Online!";
      }
      if ($totalCount == "1") {
        $playerWord = " Player";
      }else{
        $playerWord = " Players";
      }
      $maxPlayers = $serverInfo['maxPlayers'];
      if ($totalCount >= $maxPlayers) {
        $status = "Server Full";
      }
      $serverMapImage = $serverInfo["mapName"];
      $imageToDisplay_server = "img/maps/".$serverMapImage.".jpg";
      $taglineString = $totalCount.$playerWord." on ".$serverMapImage;
    } catch (Exception $e) {
      $imageToDisplay_server = "img/hero_bg03.jpg";
      $taglineString = "Server Offline!";
      $status = "Offline";
    }
    break;
}
?>
