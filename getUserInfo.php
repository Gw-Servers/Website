<?php
    if($mybb->user['uid']){
      $usersSteamID = $mybb->user['steamid'];
      $usersUniqueid = $mybb->user['uniqueid'];

      //Connection info:
      $host = "localhost";
      $dbname = "rozbarok_en_everything";
      $user = "rozbarok_mat";
      $pass = "lD;Z2[H1Q6JQ";
      try{
        $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        $STH = $DBH->prepare("SELECT * FROM en_shop WHERE steamid = :usersSteamID");
        $STH->bindParam(':usersSteamID', $usersSteamID);
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);

        while ($usersSteamIDFetch = $STH->fetch()) {
          $userSteamIDResult = $userSteamIDFetch['steamid'];
          $usersPoints = $usersSteamIDFetch['bob'];
          $usersTokens = $usersSteamIDFetch['tokens'];
        }

        $STH = $DBH->prepare("SELECT * FROM en_nobles WHERE steamid = :usersSteamID OR uniqueid = :usersUniqueid");
        $STH->bindParam(':usersSteamID', $usersSteamID);
        $STH->bindParam(':usersUniqueid', $usersUniqueid);
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($usersNobilityFetch = $STH->fetch()) {
          $usersRank = $usersNobilityFetch['nobility'];
        }

        if (!isset($usersRank) || $usersRank == "" || is_null($usersRank)) {
          $usersRank = "Member";
        }

        if (!isset($usersTokens, $usersPoints)) {
          $getUserInfo = "<div class=\"row\">
                  <div class=\"five columns\">
                    <img src=\"../forums/".$mybb->user['avatar']."\" class=\"responsive-img hide-on-break profile-img\">
                  </div>
                  <div class=\"seven columns no-margin\">
                    <h5 class=\"no-margin\" id=\"username\"><strong>".$mybb->user['username']."</strong></h5>
                    <div class=\"center-text\">Please play on a server at least once!</div>
                    <div class=\"center-text\"><a href=\"servers.php\" class=\"button button-primary\">Join a Server!</a></div>
                  </div>
                </div>";
        } else {
          $usersRank = ucfirst($usersRank);
          $getUserInfo = "<div class=\"row\">
                  <div class=\"five columns\">
                    <img src=\"../forums/".$mybb->user['avatar']."\" class=\"responsive-img hide-on-break profile-img\">
                  </div>
                  <div class=\"seven columns no-margin\">
                    <h5 class=\"no-margin\" id=\"username\"><strong>".$mybb->user['username']."</strong></h5>
                    <table class=\"u-full-width no-margin\" id=\"user-info\">
                      <tbody>
                       <tr>
                         <td class=\"no-padding\">Bob:</td>
                         <td class=\"no-padding text-align-right sub-text-dark\">".$usersPoints."</td>
                       </tr>
                       <tr>
                         <td class=\"no-padding\">Tokens:</td>
                         <td class=\"no-padding text-align-right sub-text-dark\">".$usersTokens."</td>
                       </tr>
                       <tr>
                         <td class=\"no-padding\">Rank:</td>
                         <td class=\"no-padding text-align-right sub-text-dark\">".$usersRank."</td>
                       </tr>
                      </tbody>
                    </table>
                  </div>
                </div>";
        }

      }catch (PDOException $e) {
        echo "Something went tits up.";
        file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
      }
    }else{
      $getUserInfo = "<div class=\"center-text\"><h4>Not Logged in?</h4></div>
      <div class=\"center-text\">Become a member to gain the most out of the community!</div>
      <div class=\"row center-text\">
        <a href=\"../forums/member.php?action=login\" class=\"button button-primary\">Log In!</a>
        <a href=\"../forums/member.php?action=register\" class=\"button button-primary\">Register!</a>
      </div>";
    }
  ?>
