<?php

// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
define("DEBUG", 1);

// Set to 0 once you're ready to go live
define("USE_SANDBOX", 1);


define("LOG_FILE", "./ipn.log");


// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
	$keyval = explode ('=', $keyval);
	if (count($keyval) == 2)
		$myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
	$get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
		$value = urlencode(stripslashes($value));
	} else {
		$value = urlencode($value);
	}
	$req .= "&$key=$value";
}

// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data

if(USE_SANDBOX == true) {
	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} else {
	$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}

$ch = curl_init($paypal_url);
if ($ch == FALSE) {
	return FALSE;
}

curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

if(DEBUG == true) {
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
}

// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);

// Set TCP timeout to 30 seconds
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.

//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);

$res = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
	{
	if(DEBUG == true) {	
		error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
	}
	curl_close($ch);
	exit;

} else {
		// Log the entire HTTP response if debug is switched on.
		if(DEBUG == true) {
			error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
			error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
		}
		curl_close($ch);
}

// Inspect IPN validation result and act accordingly

// Split response headers and payload, a better way for strcmp
$tokens = explode("\r\n\r\n", trim($res));
$res = trim(end($tokens));

if (strcmp ($res, "VERIFIED") == 0) {
	// this is the success statement
	// check whether the payment_status is Completed
	// check that txn_id has not been previously processed
	// check that receiver_email is your PayPal email
	// check that payment_amount/payment_currency are correct
	// process payment and mark item as paid.

	// assign posted variables to local variables
	//$item_name = $_POST['item_name'];
	//$item_number = $_POST['item_number'];
	$payment_status = $_POST['payment_status'];
	//$payment_amount = $_POST['mc_gross'];
	//$payment_currency = $_POST['mc_currency'];
	//$txn_id = $_POST['txn_id'];
	$receiver_email = $_POST['receiver_email'];
	$payer_email = $_POST['payer_email'];

	$payer_uniqueID = $_POST['custom'];
	$token_amount = $_POST['quantity'];

	if ($payment_status == "Pending") {
		//send email

		$to  = $payer_email;

		// subject
		$subject = 'Get wrecked! - Donation Pending';

		// message
		//require_once 'emails/payment_complete.php';

		$message = "<p><strong>Your donation is Pending!</strong></p>";
		$message .= "<p>Your PayPal payment is currently pending, once your payment is complete, you will recieve your tokens!</p>";
		$message .= "<p>if you have any questions, please contact Mike or Mat!.</p>";
		$message .= "<p>Thank you!</p>";
		$message .= "<p>Get wrecked! Servers</p>";

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: Get wrecked! Servers <getwreckedservers@gmail.com>' . "\r\n";
		$headers .= 'Bcc: getwreckedservers@gmail.com' . "\r\n";

		// Mail it
		mail($to, $subject, $message, $headers);
	}

	if ($payment_status == "Processed") {
		//send email

		$to  = $payer_email;

		// subject
		$subject = 'Get wrecked! - Donation Processed';

		// message
		//require_once 'emails/payment_complete.php';

		$message = "<p><strong>Your donation is Processed!</strong></p>";
		$message .= "<p>Your PayPal payment is now Processed, once your payment is complete, you will recieve your tokens!</p>";
		$message .= "<p>if you have any questions, please contact Mike or Mat!.</p>";
		$message .= "<p>Thank you!</p>";
		$message .= "<p>Get wrecked! Servers</p>";

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: Get wrecked! Servers <getwreckedservers@gmail.com>' . "\r\n";
		$headers .= 'Bcc: getwreckedservers@gmail.com' . "\r\n";

		// Mail it
		mail($to, $subject, $message, $headers);
	}

	if ($payment_status == "In-Progress") {
		//send email

		$to  = $payer_email;

		// subject
		$subject = 'Get wrecked! - Donation In Progress!';

		// message
		//require_once 'emails/payment_complete.php';

		$message = "<p><strong>Your donation is in progress!</strong></p>";
		$message .= "<p>Your PayPal payment is now in progress, once your payment is complete, you will recieve your tokens!</p>";
		$message .= "<p>if you have any questions, please contact Mike or Mat!.</p>";
		$message .= "<p>Thank you!</p>";
		$message .= "<p>Get wrecked! Servers</p>";

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: Get wrecked! Servers <getwreckedservers@gmail.com>' . "\r\n";
		$headers .= 'Bcc: getwreckedservers@gmail.com' . "\r\n";

		// Mail it
		mail($to, $subject, $message, $headers);
	}

	if ($payment_status == "Completed") {
		//update database

		//Connection info:
		$host = "localhost";
		$dbname = "rozbarok_en_everything";
		$user = "rozbarok_mat";
		$pass = "lD;Z2[H1Q6JQ";

		$dbname_forum = "rozbarok_mybbforum";

		//PDO
		try{
			$DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
			$DBH_forum = new PDO("mysql:host=$host;dbname=$dbname_forum", $user, $pass);

			$STH_forum = $DBH_forum->prepare('SELECT * from mybb_users WHERE uniqueid = :payerid');
			$STH_forum->bindParam(':payerid', $payer_uniqueID);
			$STH_forum->execute();

			$STH_forum->setFetchMode(PDO::FETCH_ASSOC);
			while ($row_forum = $STH_forum->fetch()) {
				$username = $row_forum['username'];

				$STH = $DBH->prepare('SELECT * from en_shop WHERE uniqueid = :payerid');
				$STH->bindParam(':payerid', $payer_uniqueID);
				$STH->execute();

				$STH->setFetchMode(PDO::FETCH_ASSOC);
				 
				while($row = $STH->fetch()) {
					// $STH = $DBH_forum->prepare('SELECT * from mybb_users WHERE uniqueid = :payerid');
					// $STH->execute();
				  $userPoints = $row['tokens'];
				  $tokensBefore = $row['tokens'];
					$userPoints = $userPoints + $token_amount;
					$STH = $DBH->prepare("UPDATE en_shop SET tokens = :tokens WHERE uniqueid = :payerid");
					$data = array('tokens' => $userPoints,'payerid' => $payer_uniqueID );
					$STH->execute($data);

					$STH = $DBH->prepare("INSERT INTO en_log_donations (uniqueid, payer_email, amount) VALUES (:uniqueid, :payer_email, :amount)");
					$STH->bindParam(':uniqueid', $payer_uniqueID);
					$STH->bindParam(':payer_email', $payer_email);
					$STH->bindParam(':amount', $token_amount);
					$STH->execute();

					//send email
					require 'php/phpmailer/PHPMailerAutoload.php';

					$message = file_get_contents('emails/donation-complete.php');
					$message = str_replace('%%user%%', $username, $message);
					$message = str_replace('%%tokenAmmount%%', $token_amount, $message);
					$message = str_replace('%%totaltokens%%', $userPoints, $message);

					//Create a new PHPMailer instance
					$mail = new PHPMailer;
					//Tell PHPMailer to use SMTP
					$mail->isSMTP();
					//Enable SMTP debugging
					// 0 = off (for production use)
					// 1 = client messages
					// 2 = client and server messages
					$mail->SMTPDebug = 2;
					//Ask for HTML-friendly debug output
					$mail->Debugoutput = 'html';
					//Set the hostname of the mail server
					$mail->Host = "euserver2.lithiumhosting.com";
					//Set the SMTP port number - likely to be 25, 465 or 587
					$mail->Port = 587;
					//Whether to use SMTP authentication
					//$mail->SMTPSecure = 'tls';
					$mail->SMTPAuth = true;
					//Username to use for SMTP authentication
					$mail->Username = "donations@gwservers.co.uk";
					//Password to use for SMTP authentication
					$mail->Password = "Matrossmiklinfad88";
					//Set who the message is to be sent from
					$mail->setFrom('donations@gwservers.co.uk', 'Gw Servers - Donations');
					//Set an alternative reply-to address
					$mail->addReplyTo('support@gwservers.co.uk', 'Gw Servers - Support');
					//Set who the message is to be sent to
					$mail->addAddress($payer_email);
					//Set the subject line
					$mail->Subject = 'Gw Servers - Donation Complete!';
					//Read an HTML message body from an external file, convert referenced images to embedded,
					//convert HTML into a basic plain-text alternative body
					$mail->msgHTML($message);
					//Replace the plain text body with one created manually
					$mail->AltBody = 'Thank you! Your donation is now complete, you should of recieved your tokens. - Gw Servers';
					//Attach an image file
					//$mail->addAttachment('images/phpmailer_mini.png');

					//send the message, check for errors
					if (!$mail->send()) {
					    echo "Mailer Error: " . $mail->ErrorInfo;
					    file_put_contents('EmailErrors.txt', $mail->ErrorInfo, FILE_APPEND);
					} else {
					    echo "Message sent!";
					}
				}
			}
			
			

		}catch(PDOException $e){
			echo "Something went tits up.";
			file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
		}
  	
		
	}

	
	
	if(DEBUG == true) {
		error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
	}
} else if (strcmp ($res, "INVALID") == 0) {
	// this is the failed statement
	// log for manual investigation
	// Add business logic here which deals with invalid IPN messages
	if(DEBUG == true) {
		error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
	}
}

?>